'use strict';

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var fs = require('fs')

var chatusers = []

server.listen(3500);

app.get('/', function(req, res) {
    var stream = fs.createReadStream(__dirname + '/index.html');
    stream.pipe(res);
});

io.sockets.on('connection', function(socket) {
    socket.on('new user', function(data, callback) {
        if (chatusers.indexOf(data) != -1) {
            callback(false);
        } else {
            callback(true);
            socket.chatuser = data;
            chatusers.push(socket.chatuser);
            updateNickNames();
        }
    });

    function updateNickNames() {
        io.sockets.emit('usernames', chatusers);
    }

    socket.on('send message', function(data) {
        io.sockets.emit('new message', {msg: data, user: socket.chatuser});
    });

    socket.on('disconnect', function(data) {
        if(!socket.chatuser) return;
        chatusers.splice(chatusers.indexOf(socket.chatuser), 1);
        updateNickNames();
    })
});