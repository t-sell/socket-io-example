'use strict';

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('start-dev', function () {
  let stream  = nodemon({ script: 'app.js'
         , ext: 'js html'
         , env: { 'NODE_ENV': 'development' }
                })

  stream
      .on('restart', function () {
        console.log('{status: \'Restarted!\'}');
      })
      .on('crash', function() {
        console.error('{status: \'Crashed!\'}');
         stream.emit('restart', 10); // restart the server in 10 seconds
      })
})
