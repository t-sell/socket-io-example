# Socket.io example

This is my hands-on approach to learning Socket.io after several articles and 
looking at even more examples.

I liked the use case and other tools used in this tutorial. There are plenty of videos 
and articles to help you get started with this topic.

Please use a specific commit to start off at a particular stage of building this example.

## Clone a particular commit 

???


## Install & Run

```bash
# install dependencies
npm install

# run
npm start
```

## Making changes in development mode

```bash
# run gulp task (does a restart for you when files are modified)
gulp start-dev
```


## References

[Hands-on video (if possible, complete the video series)](https://www.youtube.com/watch?v=pNKNYLv2BpQ)